use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("WELCOME TO GUESS THE NUMBER!");
    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        println!("Please enter your guess");
        let mut guess = String::new();

        io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");
        
        let guess: u32 = guess.trim().parse().expect("Please type a number!");

        println!("you guessed: {}", guess);

        match guess.cmp(&secret_number){
            Ordering::Less => println!("Guess higher!"),
            Ordering::Equal => {
                println!("You got it!");
                break;
            }
            Ordering::Greater => println!("Guess lower!"),
        }
    }
}